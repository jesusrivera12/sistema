<h2><strong>Inicio página web</strong></h2>

<a href="{{ url('empleados/create') }}">Agregar Empleados</a><br><br>


<table class="table table-light">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Documento</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Sexo</th>
            <th>Correo</th>
            <th>Dirección</th>
            <th>Clave</th>
            <th>Foto</th>
            <th>Acciones</th>
        </tr>
    </thead>

    <tbody>
    @foreach($empleados as $empleado)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{ $empleado->Documento }}</td>
            <td>{{ $empleado->Nombre }}</td>
            <td>{{ $empleado->Apellidos }}</td>
            <td>{{ $empleado->Sexo }}</td>
            <td>{{ $empleado->Correo }}</td>
            <td>{{ $empleado->Direccion }}</td>
            <td>{{ $empleado->Clave }}</td>
            <td>
            <img src="{{ asset('storage').'/'.$empleado->Foto }}" alt="" width="100">

            </td>
            <td>
            
            <a href="{{ url('/empleados/'.$empleado->id.'/edit') }}">Editar</a>

             | 
            
            <form method="post" action="{{ url('/empleados/'.$empleado->id) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }} <!-- IDENTIFICADOR para llamar al método destroyer -->
            <button type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
               
            </form></td>
        </tr>
    @endforeach
    </tbody>
</table>