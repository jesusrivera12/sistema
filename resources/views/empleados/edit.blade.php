<h2><strong>EDICIÓN DE BIBLIOTECARIO</strong></h2>

<form action="{{ url('/empleados/'.$empleado->id) }}" method="post" enctype="multipart/form-data">
{{ csrf_field() }} <!-- token-->
{{ method_field('PATCH') }} <!-- Metodo solicitado por la ruta para poder actualizar-->

    <label for="Documento">{{'Documento'}}</label>
    <input type="number" name="Documento" id="Documento" value="{{ $empleado->Documento }}"><br>

    <label for="Nombre">{{'Nombres'}}</label>
    <input type="text" name="Nombre" id="Nombre" value="{{ $empleado->Nombre }}"><br>

    <label for="Apellidos">{{'Apellidos'}}</label>
    <input type="text" name="Apellidos" id="Apellidos" value="{{ $empleado->Apellidos }}"><br>

    <label for="Sexo">{{'Sexo'}}</label>
    <input type="text" name="Sexo" id="Sexo" value="{{ $empleado->Sexo }}"><br>

    <label for="Correo">{{'Correo'}}</label>
    <input type="text" name="Correo" id="Correo" value="{{ $empleado->Correo }}"><br>

    <label for="Direccion">{{'Direccion'}}</label>
    <input type="text" name="Direccion" id="Direccion" value="{{ $empleado->Direccion }}"><br>

    <label for="Clave">{{'Clave'}}</label>
    <input type="text" name="Clave" id="Clave" value="{{ $empleado->Clave }}"><br>

    <label for="Foto">{{'Foto'}}</label>
    <br>
    <img src="{{ asset('storage').'/'.$empleado->Foto }}" alt="" width="100">
    <br>
    <input type="file" name="Foto" id="Foto" value=""><br>
    <br>
    <input type="submit" value="Editar">

    <a href="{{ url('empleados') }}">Regresar</a><br>

</form>