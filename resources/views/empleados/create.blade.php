<h2><strong>REGISTRO DE BIBLIOTECARIOS</strong></h2>
<form action="{{url('/empleados')}}" method="post" enctype="multipart/form-data">
{{csrf_field()}} 

<label for="Documento">{{'Documento'}}</label>
<input type="number" name="Documento" id="Documento" value=""><br>

<label for="Nombre">{{'Nombres'}}</label>
<input type="text" name="Nombre" id="Nombre" value=""><br>

<label for="Apellidos">{{'Apellidos'}}</label>
<input type="text" name="Apellidos" id="Apellidos" value=""><br>

<label for="Sexo">{{'Sexo'}}</label>
<input type="text" name="Sexo" id="Sexo" value=""><br>

<label for="Correo">{{'Correo'}}</label>
<input type="text" name="Correo" id="Correo" value=""><br>

<label for="Direccion">{{'Direccion'}}</label>
<input type="text" name="Direccion" id="Direccion" value=""><br>

<label for="Clave">{{'Clave'}}</label>
<input type="text" name="Clave" id="Clave" value=""><br>

<label for="Foto">{{'Foto'}}</label>
<input type="file" name="Foto" id="Foto" value=""><br>

<input type="submit" value="Agregar">

<a href="{{ url('empleados') }}">Regresar</a><br>

</form>